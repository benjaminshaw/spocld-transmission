#!/usr/bin/env python

"""
    **************************************************************************
    |                                                                        |
    |                 estimate_transmission_time.py 1.0                      |
    |                                                                        |
    **************************************************************************
    | Description:                                                           |
    |                                                                        |
    | Takes parameters describing SPOCLD data objects and computes the       |
    | time of transmission over a data link                                  |
    |                                                                        |
    **************************************************************************
    | Author: Benjamin Shaw                                                  |
    | Email : benjamin.shaw@manchester.ac.uk                                 |
    **************************************************************************
    | Required Command Line Arguments: None                                  |
    **************************************************************************
    | License:                                                               |
    |                                                                        |
    | Code made available under the GPLv3 (GNU General Public License), that |
    | allows you to copy, modify and redistribute the code as you see fit    |
    | (http://www.gnu.org/copyleft/gpl.html). Though a mention to the        |
    | original author using the citation above in derivative works, would be |
    | very much appreciated.                                                 |
    **************************************************************************
"""

import argparse
import numpy as np


class TransTime():
    """
    Parses SPOCLD data object parameters and computes an estimate
    of the transmission time for a particular transmission rate

    # pylint: disable=R0902
    # Eight is reasonable in this case.

    # pylint: disable=R0913
    # Seven is reasonable in this case.
    """

    def __init__(self, disp, freq, bandwidth, rate, nchan, tsamp):
        self.disp = disp
        self.freq = freq
        self.bandwidth = bandwidth
        self.rate = rate
        self.nchan = nchan
        self.npol = 4
        self.nbeams = 3
        self.tsamp = tsamp * 1e-6

    def calc_dm_sweep(self) -> float:
        """
        Calcualates the amount of time for one sweep of
        DM to elapse

        Parameters
        ----------
        self : object
            The object pointer.

        Returns
        ---------
        sweep_time: float
            The sweep time in seconds
        """

        sweep_time = (8.3e6 * self.bandwidth * self.disp) / self.freq**3 / 1e3
        return sweep_time

    def calc_data_vol(self, sweep: float) -> float:
        """
        Calculates the total number of samples, and therefore
        the size (in MB) of a TF block.

        Parameters
        ----------
        self : object
            The object pointer.
        sweep : float
           The DM sweep (seconds)

        Returns
        -------
        nsamples : float
           The total size of a TF block (MB)
        """

        # Calculate the number of spectra in
        # one DM sweep time
        nspectra = int(np.ceil(sweep / self.tsamp))

        # Convert the number of spectra into a number of kilobytes
        # (8 bits per sample)
        kbytes_per_channel = nspectra / 1e3

        # Convert spectra to samples (total number of samples in
        # TF block), accounting for polarisations - result in MB
        nsamples = kbytes_per_channel * self.npol * self.nchan / 1e3

        return nsamples

    def calc_tof(self, vol: float) -> float:
        """
        Calculates the time-of-flight of a volume of data
        over a link with some transfer speed.

        Parameters
        ----------
        self : object
            The object pointer.
        vol : float
            The data volume to transfer in MB.

        Returns
        -------
        tof : float
            The data transfer time.
        """

        # Convert uplink speed to MB per second
        uplink = (self.rate / 8) * 1e3

        # Multiple volume per candidate by number of beams
        # (3 beams per node)
        vol *= self.nbeams

        # Compute the time-of-flight for the data block
        tof = vol / uplink

        return tof

    def input_dict(self) -> dict:
        """
        Returns a dictionary of the input parameters

        Parameters
        ----------
        self : object
            The object pointer.

        Returns
        -------
        pars : dict
            Dictionary of input parameters
        """
        pars = {}
        pars["DM"] = self.disp
        pars["bandwidth"] = self.bandwidth
        pars["frequency"] = self.freq
        pars["tsamp"] = self.tsamp
        pars["speed"] = self.rate
        pars["beams"] = self.nbeams
        pars["pols"] = self.npol
        pars["nchan"] = self.nchan

        return pars

    def run(self):
        """
        Main method
        """
        print(self.input_dict())
        sweep_time = self.calc_dm_sweep()
        data_vol = self.calc_data_vol(sweep_time)
        trans_time = self.calc_tof(data_vol)
        print("Transmission time: {} [s] \n".format(trans_time))


def main():
    """
    Entrypoint if running from CLI
    """

    parser = argparse.ArgumentParser(
        description='SDP transmission time for SPOCLD data objects'
        )
    parser.add_argument('-d',
                        '--dm',
                        help='Candidate dispersion measure',
                        default=300,
                        type=float,
                        required=False)
    parser.add_argument('-f',
                        '--freq',
                        help='Centre frequency (MHz)',
                        default=1400,
                        type=float,
                        required=False)
    parser.add_argument('-b',
                        '--bw',
                        help='Bandwidth (MHz)',
                        default=300,
                        type=float,
                        required=False)
    parser.add_argument('-r',
                        '--rate',
                        help='Data rate (Gbps)',
                        default=1,
                        type=float,
                        required=False)
    parser.add_argument('-c',
                        '--nchan',
                        help='Number of channels',
                        default=4096,
                        type=int,
                        required=False)
    parser.add_argument('-t',
                        '--tsamp',
                        help='Sample interval (us)',
                        default=54.6,
                        type=float,
                        required=False)
    args = parser.parse_args()

    compute_time = TransTime(args.dm,
                             args.freq,
                             args.bw,
                             args.rate,
                             args.nchan,
                             args.tsamp)
    compute_time.run()


if __name__ == '__main__':
    main()
